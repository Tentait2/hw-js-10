let tabsTittle = document.body.querySelectorAll('.tabs-title')
let listItem = document.body.querySelectorAll('ul.tabs-content > li')
let list = document.body.querySelector('.tabs-content')



tabsTittle.forEach(item => {
    item.addEventListener('click', selectTabsTittle)})

tabsTittle.forEach((item , key)=> {
    item.setAttribute(`data-tab-name`, `tab-${key++}`)
})
// Я мог бы добавить их в html, но сделал тут на случай новых элементов. В любом случае мы пользуемся дата-атрибутом для работы в джс.
listItem.forEach((item , key)=> {
    item.classList.add(`tab-${key++}`)
})
// Так же в js добавляю дата атрибуты уже для текста

function selectTabsTittle(){
    tabsTittle.forEach(item => {
        item.classList.remove("tabs-title-active")
    })
    this.classList.add("tabs-title-active")
    tabName = this.dataset.tabName
    selectListItem(tabName)
}

function selectListItem(tabName){
    listItem.forEach(item =>
    {if (item.classList.contains(tabName)){
            item.classList.add('list-item-active')
            item.classList.remove('list-item-inactive')
            list.prepend(item)
            // Сделано чтобы текст показывался удобно сверху
        } else {
            item.classList.remove('list-item-active')
            item.classList.add('list-item-inactive')
        }})
}

